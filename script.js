// Assign Elements
const cells = document.querySelectorAll(".cell");
const message = document.querySelector("#message");
const newGameBtn = document.querySelector("#new-game-btn");
const result = document.querySelector("#result");
const resultMessage = document.querySelector("#result-message");


// Event Listeners
cells.forEach(cell => {
    cell.addEventListener("click", (e) => {
        board.newMove(e);
    }, false);
})

newGameBtn.addEventListener("click", newGame, false);


// Setup
const winningCombinations = [
    [1,2,3],
    [4,5,6],
    [7,8,9],
    [1,4,7],
    [2,5,8],
    [3,6,9],
    [1,5,9],
    [3,5,7]
]
let board;


// Board Setup
function Board() {
    this.player1 = {
        name: "Player One",
        mark: `<div class="x"></div>`,
        moves : []
    }
    this.player2 = {
        name: "Player Two",
        mark: `<div class="o"></div>`,
        moves : []
    }
    
    this.currentPlayer = this.player1;
    this.isPlayer1 = true;

    Board.prototype.checkWinner = () => {
        return winningCombinations.some(combination => {
            return combination.every(number => {
                return this.currentPlayer.moves.includes(number);
            })
        })
    }

    Board.prototype.changePlayer = () => {
        // Check For Winner
        if(this.checkWinner()) {
            resultMessage.innerText = `${this.currentPlayer.name} wins!`
            result.classList.add("show");
            message.innerHTML = `We have a winner!`;

        // Check For Draw
        } else if(this.player1.moves.length + this.player2.moves.length >= 9) {
            resultMessage.innerText = `Draw!`
            result.classList.add("show");
            message.innerHTML = `It's a Draw!`;

        } else {
            if(this.currentPlayer.name == "Player One") {
                this.currentPlayer = this.player2;
            } else {
                this.currentPlayer = this.player1;
            }
            message.innerHTML = `It's ${this.currentPlayer.name}'s turn`;
        }
    }

    Board.prototype.newMove = (e) => {
        if(!e.target.disabled) {
            e.target.innerHTML = this.currentPlayer.mark;
            e.target.disabled = true;
            e.target.classList.add("not-available");
            this.currentPlayer.moves.push(parseInt(e.target.dataset.value));

            this.changePlayer();
        }
    }
}


// Functions
function newGame() {
    cells.forEach(cell => {
        result.classList.remove("show");
        cell.disabled = false;
        cell.innerHTML = "";
        cell.classList.remove("not-available");
    })

    delete board;
    board = new Board;
}


// Initialize
newGame();